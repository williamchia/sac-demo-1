var echo = require('./echo');
var assert = require('assert');

describe('test case', function() {
    it('message', function(done){
        var message = "Hello from the Serverless confrence in the beautiful the Hague";
        assert.equal(JSON.stringify(message), echo(message));
        done();
    })
});
